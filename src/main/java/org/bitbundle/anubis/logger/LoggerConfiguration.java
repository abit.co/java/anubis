package org.bitbundle.anubis.logger;

import io.sentry.Sentry;
import io.sentry.dsn.InvalidDsnException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.RootLoggerComponentBuilder;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;
import org.bitbundle.anubis.http.config.AnubisConfig;

public class LoggerConfiguration {
    private static AnubisConfig config = AnubisConfig.getInstance();
    private static RootLoggerComponentBuilder rootLogger;
    static Configuration newConfiguration(final String name, ConfigurationBuilder<BuiltConfiguration> builder) {
        builder.setConfigurationName(name);
        builder.setStatusLevel(Level.ERROR);
        createRootLogger(builder);

        if (config.isConsoleAppenderEnabled()) {
            addConsoleAppender(builder);
        }
        if (config.isSentryAppenderEnabled()) {
            addSentryAppender(builder);
        }
        if (config.isKafkaAppenderEnabled()) {
            addKafkaAppender(builder);
        }

        return builder.add(rootLogger).build();
    }

    private static void createRootLogger(ConfigurationBuilder<BuiltConfiguration> builder) {
        if (config.isRootLoggerAsync()) {
            rootLogger = builder.newAsyncRootLogger(config.getRootLoggerLevel());
        } else {
            rootLogger = builder.newRootLogger(config.getRootLoggerLevel());
        }
        rootLogger.addAttribute("includeLocation", config.isRootLoggerIncludeLocation());
    }

    private static void addConsoleAppender(ConfigurationBuilder<BuiltConfiguration> builder) {
        AppenderComponentBuilder appender = builder.newAppender("Stdout", "Console");
        appender.addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT)
                .add(builder.newLayout("PatternLayout")
                        .addAttribute("pattern", config.getConsoleAppenderPattern()));
        builder.add(appender);
        rootLogger.add(builder.newAppenderRef("Stdout"));
    }

    private static void addSentryAppender(ConfigurationBuilder<BuiltConfiguration> builder) {
        try {
            Sentry.init(config.getSentryAppenderDsn());

            AppenderComponentBuilder appender = builder.newAppender("Sentry", "Sentry");
            builder.add(appender);
            rootLogger.add(builder.newAppenderRef("Sentry").addAttribute("level", config.getSentryAppenderLevel()));
        } catch (InvalidDsnException e) {
            e.printStackTrace();
        }
    }

    private static void addKafkaAppender(ConfigurationBuilder<BuiltConfiguration> builder) {
        // Check for potential problems
        String servers = config.getKafkaAppenderBootstrapServers();
        String topic = config.getKafkaAppenderTopic();
        if (servers == null || servers.isEmpty()
                || topic == null || topic.isEmpty()) {
            throw new NullPointerException("bootstrap.servers and topic must be defined");
        }

        AppenderComponentBuilder appender = builder.newAppender("Kafka", "Kafka");
        appender.add(builder.newLayout("PatternLayout").addAttribute("pattern", config.getKafkaAppenderPattern()));
        appender.addComponent(builder.newComponent("bootstrap.servers", "Property", servers));
        appender.addAttribute("topic", topic);
        builder.add(appender);

        rootLogger.add(builder.newAppenderRef("Kafka").addAttribute("level", config.getKafkaAppenderLevel()));
    }
}
