package org.bitbundle.anubis.logger;

import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.ConfigurationFactory;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Order;
import org.apache.logging.log4j.core.config.plugins.Plugin;

import java.net.URI;

@Plugin(name = "AnubisConfigurationFactory", category = ConfigurationFactory.CATEGORY) @Order(50)
public class AnubisConfigurationFactory extends ConfigurationFactory {

    @Override
    public Configuration getConfiguration(final LoggerContext loggerContext, final ConfigurationSource source) {
        return getConfiguration(loggerContext, source.toString(), null);
    }

    @Override
    public Configuration getConfiguration(final LoggerContext loggerContext, final String name, final URI configLocation) {
        return LoggerConfiguration.newConfiguration(name, newConfigurationBuilder());
    }

    @Override
    protected String[] getSupportedTypes() {
        return new String[] {"*"};
    }
}
