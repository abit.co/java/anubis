package org.bitbundle.anubis.http.config;

import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.Level;
import org.bitbundle.util.SmartResourceBundle;

import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;

@Setter @Getter
public class AnubisConfig {
    public static String defaultBaseName = "application";
    private static Map<String, AnubisConfig> instances = new HashMap<>();
    private SmartResourceBundle resourceBundle;

    private final String patternConsoleSimple = "[%d{ISO8601}][%highlight{%5p}] --- [%X{trace-id}] %msg%n";
    private final String patternConsoleFull = "[%d{ISO8601}][%highlight{%5p}][%style{%pid}{magenta}] --- [%t][%style{%C{1.}}{cyan}:%style{%-7.-10L}{cyan}][%X{trace-id}] %msg%n";
    private final String patternIOSimple = "[%d{ISO8601}][%level][%X{trace-id}] %msg%n";
    private final String patternIOFull = "[%d{ISO8601}][%level][%pid][%t][%c{1.}][%X{trace-id}] %msg%n";

    private boolean rootLoggerAsync = true;
    private boolean rootLoggerIncludeLocation = false;
    private Level rootLoggerLevel = Level.TRACE;
    private boolean consoleAppenderEnabled = true;
    private String consoleAppenderPattern = patternConsoleSimple;

    private boolean sentryAppenderEnabled = false;
    private String sentryAppenderDsn;
    private Level sentryAppenderLevel = Level.ERROR;

    private boolean kafkaAppenderEnabled = false;
    private String kafkaAppenderTopic;
    private String kafkaAppenderBootstrapServers;
    private Level kafkaAppenderLevel = Level.TRACE;
    private String kafkaAppenderPattern = patternIOSimple;

    public AnubisConfig() {
        setUp(defaultBaseName);
    }

    public AnubisConfig(String baseName) {
        setUp(baseName);
    }

    private void setUp(String baseName) {
        try {
            resourceBundle = SmartResourceBundle.getBundle(baseName);
        } catch (MissingResourceException e) {
            e.printStackTrace();
            return;
        }

        if (resourceBundle.containsKey("anubis.root.async")) {
            rootLoggerAsync = resourceBundle.getBoolean("anubis.root.async");
        }
        if (resourceBundle.containsKey("anubis.root.include_location")) {
            rootLoggerIncludeLocation = resourceBundle.getBoolean("anubis.root.include_location");
        }
        if (isRootLoggerIncludeLocation()) {
            consoleAppenderPattern = patternConsoleFull;
            kafkaAppenderPattern = patternIOFull;
        }
        if (resourceBundle.containsKey("anubis.root.level")) {
            rootLoggerLevel = Level.getLevel(resourceBundle.getString("anubis.root.level"));
        }

        if (resourceBundle.containsKey("anubis.console.enabled")) {
            consoleAppenderEnabled = resourceBundle.getBoolean("anubis.console.enabled");
        }
        if (resourceBundle.containsKey("anubis.console.pattern")) {
            consoleAppenderPattern = resourceBundle.getString("anubis.console.pattern");
        }

        sentryAppenderDsn = resourceBundle.getString("anubis.sentry.dsn");
        if (resourceBundle.containsKey("anubis.sentry.enabled")) {
            sentryAppenderEnabled = resourceBundle.getBoolean("anubis.sentry.enabled");
        }
        if (resourceBundle.containsKey("anubis.sentry.level")) {
            sentryAppenderLevel = Level.getLevel(resourceBundle.getString("anubis.sentry.level"));
        }

        kafkaAppenderBootstrapServers = resourceBundle.getString("anubis.kafka.bootstrap_servers");
        kafkaAppenderTopic = resourceBundle.getString("anubis.kafka.topic");
        if (resourceBundle.containsKey("anubis.kafka.enabled")) {
            kafkaAppenderEnabled = resourceBundle.getBoolean("anubis.kafka.enabled");
        }
        if (resourceBundle.containsKey("anubis.kafka.level")) {
            kafkaAppenderLevel = Level.getLevel(resourceBundle.getString("anubis.kafka.level"));
        }
        if (resourceBundle.containsKey("anubis.kafka.pattern")) {
            kafkaAppenderPattern = resourceBundle.getString("anubis.kafka.pattern");
        }
    }

    public static AnubisConfig getInstance(String baseName) {
        AnubisConfig instance = instances.get(baseName);
        if (instance == null) {
            instance = new AnubisConfig(baseName);
            instances.put(baseName, instance);
        }

        return instance;
    }

    public static AnubisConfig getInstance() {
        return getInstance(defaultBaseName);
    }
}
