package org.bitbundle.anubis.http.consumer;

import java.util.Objects;

@FunctionalInterface
public interface HttpConsumer<T, U, V> {
    void accept(T t, U u, V v);
    default HttpConsumer<T, U, V> andThen(HttpConsumer<? super T, ? super U, ? super V> after) {
        Objects.requireNonNull(after);

        return (l, r, s) -> {
            accept(l, r, s);
            after.accept(l, r, s);
        };
    }
}
