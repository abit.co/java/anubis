package org.bitbundle.anubis.http.interceptor;

import io.sentry.Sentry;
import io.sentry.SentryClient;
import io.sentry.context.Context;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.ThreadContext;
import org.bitbundle.anubis.http.config.AnubisConfig;
import org.bitbundle.anubis.http.consumer.HttpConsumer;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class LoggerInterceptor extends HandlerInterceptorAdapter {
    private AnubisConfig config;
    private List<HttpConsumer<HttpServletRequest, HttpServletResponse, String>> consumers;

    {
        config = AnubisConfig.getInstance();
        consumers = new ArrayList<>();
    }

    public LoggerInterceptor() {
        setUp();
    }

    public LoggerInterceptor(AnubisConfig config) {
        this.config = config;
        setUp();
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uuid = UUID.randomUUID().toString();
        ThreadContext.put("trace-id", uuid);
        request.setAttribute("trace-id", uuid);
        response.addHeader("Trace-Id", uuid);

        consume(request, response, uuid);
        return super.preHandle(request, response, handler);
    }

    private void consume(HttpServletRequest request, HttpServletResponse response, String uuid) {
        consumers.forEach(consumer -> consumer.accept(request, response, uuid));
    }

    public void setUp() {
        if (config.isSentryAppenderEnabled()) {
            setUpSentry();
        }
    }

    private void setUpSentry() {
        consumers.add((request, response, uuid) -> {
            SentryClient sentry = Sentry.getStoredClient();
            Context context = sentry.getContext();
            context.addTag("trace-id", uuid);
        });
    }
}
