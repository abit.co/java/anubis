package org.bitbundle.anubis.http.exception;

import org.springframework.http.HttpStatus;

public class BadRequestException extends HttpException {
    private static String errorCode = "000.000.003";
    private static String errorMessage = "Bad request: invalid data or format";
    private static HttpStatus errorStatus = HttpStatus.BAD_REQUEST;

    public BadRequestException() {
        super(errorCode, errorMessage, errorStatus);
    }

    public BadRequestException(Exception cause) {
        super(errorCode, errorMessage, errorStatus, cause);
    }
}
