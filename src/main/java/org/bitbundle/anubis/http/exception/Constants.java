package org.bitbundle.anubis.http.exception;

public interface Constants {
    // Http Exception Codes:
    // InternalServerException: 000.000.000
    // NotFoundException: 000.000.001
    // UnAuthorizedException: 000.000.002
    // BadRequestException: 000.000.003
    // ForbiddenException: 000.000.004
}
