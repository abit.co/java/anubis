package org.bitbundle.anubis.http.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends HttpException {
    private static String errorCode = "000.000.001";
    private static String errorMessage = "Resource could not be found";
    private static HttpStatus errorStatus = HttpStatus.NOT_FOUND;

    public NotFoundException() {
        super(errorCode, errorMessage, errorStatus);
    }
}
