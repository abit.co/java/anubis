package org.bitbundle.anubis.http.exception;

import org.springframework.http.HttpStatus;

public class UnAuthorizedException extends HttpException {
    private static String errorCode = "000.000.002";
    private static String errorMessage = "Request owner is not authorized";
    private static HttpStatus errorStatus = HttpStatus.UNAUTHORIZED;

    public UnAuthorizedException() {
        super(errorCode, errorMessage, errorStatus);
    }

    public UnAuthorizedException(Exception cause) {
        super(errorCode, errorMessage, errorStatus, cause);
    }
}
