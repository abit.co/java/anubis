package org.bitbundle.anubis.http.exception;

import org.springframework.http.HttpStatus;

public class ForbiddenException extends HttpException {
    private static String errorCode = "000.000.004";
    private static String errorMessage = "Forbidden: resource is not granted";
    private static HttpStatus errorStatus = HttpStatus.FORBIDDEN;

    public ForbiddenException() {
        super(errorCode, errorMessage, errorStatus);
    }
}
