package org.bitbundle.anubis.http.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter @Setter
public class HttpException extends RuntimeException {
    private HttpStatus status;
    private Body body;
    private Exception cause;

    @Getter @Setter
    @NoArgsConstructor
    private class Body {
        private String code;
        private String message;

        @JsonFormat(shape = JsonFormat.Shape.STRING)
        private LocalDateTime timestamp = LocalDateTime.now();

        public Body(String code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    private HttpException() {
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public HttpException(HttpStatus status) {
        this();
        this.status = status;
    }

    public HttpException(String code, String message) {
        this();
        this.body = new Body(code, message);
    }

    public HttpException(String code, String message, HttpStatus status) {
        this(status);
        this.body = new Body(code, message);
    }

    public HttpException(String code, String message, HttpStatus status, Exception cause) {
        this(code, message, status);
        this.cause = cause;
    }
}
