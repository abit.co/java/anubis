package org.bitbundle.anubis.http.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbundle.anubis.http.consumer.HttpConsumer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class HttpExceptionHandler {
    private final Logger logger = LogManager.getLogger(HttpExceptionHandler.class);

    /**
     * Some place cannot use @ExceptionHandler, e.g, Spring security filter
     * We need another way to handle those cases, thus this method would
     * return a consumer to achieve it
     *
     * @see HttpConsumer
     * @return an instance of HttpConsumer
     */
    @Bean
    public HttpConsumer<Exception, HttpServletRequest, HttpServletResponse> httpExceptionConsumer() {
        return (ex, request, response) -> {
            ResponseEntity<Object> entity = handleException(ex, new ServletWebRequest(request));
            response.setStatus(entity.getStatusCodeValue());

            Map<String, String> headers = entity.getHeaders().toSingleValueMap();
            for (Map.Entry<String, String> header : headers.entrySet()) {
                response.setHeader(header.getKey(), header.getValue());
            }

            if (!response.containsHeader(HttpHeaders.CONTENT_TYPE)) {
                response.setContentType(MediaType.APPLICATION_JSON_UTF8.toString());
            }

            ObjectMapper om = new ObjectMapper();
            om.findAndRegisterModules();
            try {
                om.writeValue(response.getWriter(), entity.getBody());
            } catch (IOException e) {
                logger.error(e);
            }
        };
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception ex, WebRequest request) {
        if (ex instanceof HttpException) {
            return handleHttpException((HttpException) ex, request);
        }

        return handleUnknownException(ex, request);
    }

    protected ResponseEntity<Object> handleHttpException(HttpException ex, WebRequest request) {
        try {
            Exception cause = ex.getCause();
            if (cause == null) {
                logger.error(ex);
            } else {
                logger.error(cause);
            }
        } catch (Exception e){
            // should not block response
        }
        return new ResponseEntity<>(ex.getBody(), null, ex.getStatus());
    }

    protected ResponseEntity<Object> handleUnknownException(Exception ex, WebRequest request) {
        HttpException httpException = new InternalServerException(ex);
        if (ex instanceof HttpMessageConversionException
                || ex instanceof MethodArgumentNotValidException) {
            httpException = new BadRequestException(ex);
        }

        return handleHttpException(httpException, request);
    }
}
