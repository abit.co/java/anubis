package org.bitbundle.anubis.http.exception;

import org.springframework.http.HttpStatus;

public class InternalServerException extends HttpException {
    private static String errorCode = "000.000.000";
    private static String errorMessage = "An error has been occurred";
    private static HttpStatus errorStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    public InternalServerException() {
        super(errorCode, errorMessage, errorStatus);
    }

    public InternalServerException(Exception cause) {
        super(errorCode, errorMessage, errorStatus, cause);
    }
}
