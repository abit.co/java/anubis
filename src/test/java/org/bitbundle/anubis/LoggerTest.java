package org.bitbundle.anubis;

import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbundle.anubis.http.config.AnubisConfig;

public class LoggerTest extends TestCase {
    static {
        AnubisConfig.defaultBaseName = "test";
    }
    private static final Logger logger = LogManager.getLogger(LoggerTest.class);
    public void testLogger() {
        logger.warn("Some error");
        logger.info("Some error");
        logger.debug("Some error");
        logger.error("Some error!!");

        Thread t = new Thread(() -> logger.info("Inside a thread"));
        t.start();
    }

    public void testLoggerFormattedMessage() {
        logger.info("{}", "a string");
    }
}
